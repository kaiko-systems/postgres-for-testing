DOCKER_LOCAL_TAG ?= local-pg-for-testing
PG_BASE_IMAGE ?= postgres
PG_BASE_IMAGE_TAG ?= 15-alpine
docker/build:
	@docker build -t $(DOCKER_LOCAL_TAG) \
           --build-arg="PG_BASE_IMAGE=$(PG_BASE_IMAGE)" \
           --build-arg="PG_BASE_IMAGE_TAG=$(PG_BASE_IMAGE_TAG)" \
           -f Dockerfile .
.PHONY: docker/build

DOCKER_RUN_EXTRA_ARGS ?= --shm-size 1.5g -e POSTGRES_USER=$(USER) -e POSTGRES_PASSWORD=$(USER)
DOCKER_RUN_CMD ?=
docker/run: docker/build
	@docker run --rm $(DOCKER_RUN_EXTRA_ARGS) -it $(DOCKER_LOCAL_TAG) $(DOCKER_RUN_CMD)
.PHONY: docker/run
