# Postgres image for Testing

**ACHTUNG: Do not use this in production! Your data will be lost!**

This image is based on Postgres 12, the one we us in Scaleway. We setted here some [Non-Durable Settings](https://www.postgresql.org/docs/current/non-durability.html) by configuring postgres as explained in [their Docker page](https://hub.docker.com/_/postgres) in the "Database Configuration" section.
