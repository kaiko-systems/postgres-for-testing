ARG PG_BASE_IMAGE=postgres
ARG PG_BASE_IMAGE_TAG=15-alpine

FROM $PG_BASE_IMAGE:$PG_BASE_IMAGE_TAG

COPY postgresql.conf /etc/postgresql/postgresql.conf
COPY initialization/extensions.sh /docker-entrypoint-initdb.d

ENTRYPOINT ["docker-entrypoint.sh"]
EXPOSE 5432
CMD ["postgres"]
