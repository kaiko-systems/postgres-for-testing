#!/bin/sh

psql -d template1 -U $POSTGRES_USER -c "CREATE EXTENSION IF NOT EXISTS pg_trgm;"
psql -d template1 -U $POSTGRES_USER -c "CREATE EXTENSION IF NOT EXISTS unaccent;"
